# Persian translation of gnome-utils.
# Copyright (C) 2011 Iranian Free Software Users Group (IFSUG.org)translation team.
# Copyright (C) 2003, 2005, 2006 Sharif FarsiWeb, Inc.
# This file is distributed under the same license as the gnome-utils package.
# Roozbeh Pournader <roozbeh@farsiweb.info>, 2003, 2005.
# Behnam Esfahbod <behnam@farsiweb.info>, 2005.
# Sara Khalatbari <sara@bamdad.org>, 2005.
# Meelad Zakaria <meelad@farsiweb.info>, 2005, 2006.
# Sanaz shahrokni <sanaz.shahrokni@gmail.com>, 2006.
# Arash Mousavi <mousavi.arash@gmail.com>, 2011-2017.
# Ali Bagheri <alijobemail@gmail.com>, 2012.
# Danial Behzadi <dani.behzadi@ubuntu.com>, 2018-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-utils HEAD\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/baobab/issues\n"
"POT-Creation-Date: 2021-02-21 14:17+0000\n"
"PO-Revision-Date: 2021-03-04 00:20+0000\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Persian\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Generator: Poedit 2.4.2\n"

#: data/org.gnome.baobab.appdata.xml.in:6 data/org.gnome.baobab.desktop.in:3
#: data/ui/baobab-main-window.ui:66 src/baobab-window.vala:292
msgid "Disk Usage Analyzer"
msgstr "تحلیلگر مصرف دیسک"

#: data/org.gnome.baobab.appdata.xml.in:7 data/org.gnome.baobab.desktop.in:4
msgid "Check folder sizes and available disk space"
msgstr "بررسی اندازهٔ شاخه‌ها و فضای دیسک موجود"

#: data/org.gnome.baobab.appdata.xml.in:9
msgid ""
"A simple application to keep your disk usage and available space under "
"control."
msgstr "برنامه‌ای ساده برای واپایش مصرف و فضای موجود دیسک."

#: data/org.gnome.baobab.appdata.xml.in:12
msgid ""
"Disk Usage Analyzer can scan specific folders, storage devices and online "
"accounts. It provides both a tree and a graphical representation showing the "
"size of each folder, making it easy to identify where disk space is wasted."
msgstr ""
"تحلیلگر مصرف دیسک می‌تواند شاخه‌های خاص، دستگاه‌های ذخیره و حساب‌های برخط را "
"بپوید. این برنامه یک بازنمایی درختی و گرافیکی فراهم می‌کند که اندازهٔ هر شاخه "
"را برای تسهیل تشخیص مکان هدر رفت فضای دیسک، نشان می‌دهد."

#: data/org.gnome.baobab.appdata.xml.in:23
msgid "Devices and Locations"
msgstr "افزاره‌ها و موقعیت‌ها"

#: data/org.gnome.baobab.appdata.xml.in:38
msgid "The GNOME Project"
msgstr "پروژه گنوم"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! The list MUST also end with a semicolon!
#: data/org.gnome.baobab.desktop.in:6
msgid "storage;space;cleanup;"
msgstr "storage;space;cleanup;ذخیره;فضا;پاکسازی;"

#: data/org.gnome.baobab.gschema.xml:9
msgid "Excluded locations URIs"
msgstr "نشانی موقعیت‌های مستثنا"

#: data/org.gnome.baobab.gschema.xml:10
msgid "A list of URIs for locations to be excluded from scanning."
msgstr "فهرستی از نشانی‌های موقعیت‌هایی که پویش نمی‌شوند."

#: data/org.gnome.baobab.gschema.xml:20
msgid "Active Chart"
msgstr "نمودار فعال"

#: data/org.gnome.baobab.gschema.xml:21
msgid "Which type of chart should be displayed."
msgstr "چه نوع نموداری باید نمایش داده شود."

#: data/org.gnome.baobab.gschema.xml:25
msgid "Window size"
msgstr "اندازهٔ پنجره"

#: data/org.gnome.baobab.gschema.xml:26
msgid "The initial size of the window"
msgstr "اندازهٔ نخستین پنجره"

#: data/org.gnome.baobab.gschema.xml:30
msgid "Window state"
msgstr "وضعیت پنجره"

#: data/org.gnome.baobab.gschema.xml:31
msgid "The GdkWindowState of the window"
msgstr "‫GdkWindowState پنجره"

#: data/gtk/help-overlay.ui:13
msgctxt "shortcut window"
msgid "General"
msgstr "عمومی"

#: data/gtk/help-overlay.ui:18
msgctxt "shortcut window"
msgid "Show help"
msgstr "نمایش راهنما"

#: data/gtk/help-overlay.ui:25
msgctxt "shortcut window"
msgid "Show / Hide primary menu"
msgstr "نمایش / نهفتن فهرست اصلی"

#: data/gtk/help-overlay.ui:31
msgctxt "shortcut window"
msgid "Quit"
msgstr "خروج"

#: data/gtk/help-overlay.ui:38
msgctxt "shortcut window"
msgid "Show Keyboard Shortcuts"
msgstr "نمایش میان‌برهای صفحه‌کلید"

#: data/gtk/help-overlay.ui:46
msgctxt "shortcut window"
msgid "Go back to location list"
msgstr "بازگشت به فهرست موقعیت‌ها"

#: data/gtk/help-overlay.ui:54
msgctxt "shortcut window"
msgid "Scanning"
msgstr "در حال پویش"

#: data/gtk/help-overlay.ui:59
msgctxt "shortcut window"
msgid "Scan folder"
msgstr "پویش شاخه"

#: data/gtk/help-overlay.ui:66
msgctxt "shortcut window"
msgid "Rescan current location"
msgstr "بازپویش موقعیت جاری"

#: data/gtk/menus.ui:7 data/ui/baobab-main-window.ui:42
msgid "_Open Folder"
msgstr "_گشودن شاخه"

#: data/gtk/menus.ui:11 data/ui/baobab-main-window.ui:51
msgid "_Copy Path to Clipboard"
msgstr "_رونوشت از مسیر"

#: data/gtk/menus.ui:15 data/ui/baobab-main-window.ui:60
msgid "Mo_ve to Trash"
msgstr "_انداختن در زباله‌دان"

#: data/gtk/menus.ui:21
msgid "Go to _parent folder"
msgstr "به شاخه _والد برو"

#: data/gtk/menus.ui:27
msgid "Zoom _in"
msgstr "_بزرگ‌نمایی"

#: data/gtk/menus.ui:31
msgid "Zoom _out"
msgstr "_کوچک‌نمایی"

#: data/ui/baobab-folder-display.ui:16 data/ui/baobab-main-window.ui:267
msgid "Folder"
msgstr "شاخه"

#: data/ui/baobab-folder-display.ui:39 data/ui/baobab-main-window.ui:295
msgid "Size"
msgstr "اندازه"

#: data/ui/baobab-folder-display.ui:55 data/ui/baobab-main-window.ui:311
msgid "Contents"
msgstr "محتویات"

#: data/ui/baobab-folder-display.ui:71 data/ui/baobab-main-window.ui:327
msgid "Modified"
msgstr "تغییر یافته"

#: data/ui/baobab-location-list.ui:18
msgid "This Computer"
msgstr "این رایانه"

#: data/ui/baobab-location-list.ui:45
msgid "Remote Locations"
msgstr "مکان‌های دوردست"

#: data/ui/baobab-preferences-dialog.ui:8 data/ui/baobab-main-window.ui:17
msgid "Preferences"
msgstr "ترجیحات"

#: data/ui/baobab-preferences-dialog.ui:17
msgid "Locations to Ignore"
msgstr "موقعیت‌های چشم‌پوشی‌شده"

#: data/ui/baobab-main-window.ui:7
msgid "Scan Folder…"
msgstr "پویش شاخه…"

#: data/ui/baobab-main-window.ui:11
msgid "Clear Recent List"
msgstr "پاک‌سازی فهرست اخیر"

#: data/ui/baobab-main-window.ui:21
msgid "Keyboard _Shortcuts"
msgstr "_میان‌برهای صفحه‌کلید"

#: data/ui/baobab-main-window.ui:25
msgid "_Help"
msgstr "_راهنما"

#: data/ui/baobab-main-window.ui:29
msgid "_About Disk Usage Analyzer"
msgstr "_دربارهٔ تحلیل‌گر مصرف دیسک"

#: data/ui/baobab-main-window.ui:85
msgid "Go back to location list"
msgstr "بازگشت به فهرست موقعیت‌ها"

#: data/ui/baobab-main-window.ui:106
msgid "Rescan current location"
msgstr "بازپویش موقعیت جاری"

#: data/ui/baobab-main-window.ui:196
msgid "Close"
msgstr "بستن"

#: data/ui/baobab-main-window.ui:372
msgid "Rings Chart"
msgstr "نمودار حلقه‌ای"

#: data/ui/baobab-main-window.ui:384
msgid "Treemap Chart"
msgstr "نمودار درختی"

#: src/baobab-application.vala:33
msgid ""
"Do not skip directories on different file systems. Ignored if DIRECTORY is "
"not specified."
msgstr ""
"از شاخه‌ها روی سامانه پرونده‌های مختلف پریده نشود. در صورت مشخّص نبودن DIRECTORY "
"نادیده گرفته می‌شود."

#: src/baobab-application.vala:34
msgid "Print version information and exit"
msgstr "چاپ اطلاعات نگارش و خروج"

#: src/baobab-cellrenderers.vala:34
#, c-format
msgid "%d item"
msgid_plural "%d items"
msgstr[0] "%Id مورد"

#. Translators: when the last modified time is unknown
#: src/baobab-cellrenderers.vala:40 src/baobab-location-list.vala:80
msgid "Unknown"
msgstr "نامشخص"

#. Translators: when the last modified time is today
#: src/baobab-cellrenderers.vala:48
msgid "Today"
msgstr "امروز"

#. Translators: when the last modified time is "days" days ago
#: src/baobab-cellrenderers.vala:53
#, c-format
msgid "%lu day"
msgid_plural "%lu days"
msgstr[0] "%lu روز"

#. Translators: when the last modified time is "months" months ago
#: src/baobab-cellrenderers.vala:58
#, c-format
msgid "%lu month"
msgid_plural "%lu months"
msgstr[0] "%lu ماه"

#. Translators: when the last modified time is "years" years ago
#: src/baobab-cellrenderers.vala:62
#, c-format
msgid "%lu year"
msgid_plural "%lu years"
msgstr[0] "%lu سال"

#: src/baobab-location-list.vala:67
#, c-format
msgid "%s Total"
msgstr "%s مجموع"

#: src/baobab-location-list.vala:71
#, c-format
msgid "%s Available"
msgstr "%s موجود"

#. useful for some remote mounts where we don't know the
#. size but do have a usage figure
#: src/baobab-location-list.vala:85
#, c-format
msgid "%s Used"
msgstr "%s استفاده‌شده"

#: src/baobab-location-list.vala:87
msgid "Unmounted"
msgstr "پیاده‌شده"

#: src/baobab-location.vala:72
msgid "Home Folder"
msgstr "شاخهٔ خانه"

#: src/baobab-location.vala:107
msgid "Computer"
msgstr "رایانه"

#. The only activatable row is "Add location"
#: src/baobab-preferences-dialog.vala:53
msgid "Select Location to Ignore"
msgstr "موقعیت‌ها را برای چشم‌پوشی برگزینید"

#: src/baobab-preferences-dialog.vala:55 src/baobab-window.vala:207
msgid "_Cancel"
msgstr "_لغو"

#: src/baobab-preferences-dialog.vala:56 src/baobab-window.vala:208
msgid "_Open"
msgstr "_گشودن"

#: src/baobab-preferences-dialog.vala:91
msgid "Add Location…"
msgstr "افزودن موقعیت…"

#: src/baobab-window.vala:205
msgid "Select Folder"
msgstr "گزینش شاخه"

#: src/baobab-window.vala:214
msgid "Recursively analyze mount points"
msgstr "تحلیل بازگشتی نقاط اتصال"

#: src/baobab-window.vala:236
msgid "Could not analyze volume."
msgstr "پویش حجم امکان‌پذیر نبود."

#: src/baobab-window.vala:273
msgid "Failed to show help"
msgstr "شکست در نمایش راهنما"

#: src/baobab-window.vala:295
msgid "A graphical tool to analyze disk usage."
msgstr "ابزاری گرافیکی برای تحلیل مصرف دیسک."

#: src/baobab-window.vala:301
msgid "translator-credits"
msgstr ""
"آرش موسوی <mousavi.arash@gmail.com>\n"
"دانیال بهزادی <dani.behzi@ubuntu.com>\n"
"روزبه پورنادر <roozbeh@farsiweb.info>\n"
"بهنام اسفهبد <behnam@farsiweb.info>\n"
"سارا خلعت‌بری <sara@bamdad.org>\n"
"میلاد زکریا <meelad@farsiweb.info>\n"
"ساناز شاه‌رکنی <sanaz.shahrokni@gmail.com>"

#: src/baobab-window.vala:375
msgid "Failed to open file"
msgstr "شکست در گشودن پرونده"

#: src/baobab-window.vala:392
msgid "Failed to move file to the trash"
msgstr "شکست در انتقال پرونده به زباله‌دان"

#: src/baobab-window.vala:603
msgid "Devices & Locations"
msgstr "افزاره‌ها و موقعیت‌ها"

#: src/baobab-window.vala:655
#, c-format
msgid "Could not scan folder “%s”"
msgstr "پویش حجم «%s» امکان‌پذیر نبود"

#: src/baobab-window.vala:670
msgid "Could not always detect occupied disk sizes."
msgstr "نمی‌توان همیشه اندازه‌های اشغالی دیسک را یافت."

#: src/baobab-window.vala:670
msgid "Apparent sizes may be shown instead."
msgstr "ممکن است در عوض، اندازه‌های ظاهری نمایش داده شوند."

#: src/baobab-window.vala:674
msgid "Scan completed"
msgstr "پویش کامل شد"

#: src/baobab-window.vala:675
#, c-format
msgid "Completed scan of “%s”"
msgstr "پویش «%s» کامل شد"

#: src/baobab-window.vala:715
#, c-format
msgid "“%s” is not a valid folder"
msgstr "شاخهٔ «%s» معتبر نیست"

#: src/baobab-window.vala:716
msgid "Could not analyze disk usage."
msgstr "تحلیل مصرف دیسک ممکن نیست."

#~ msgid "Baobab"
#~ msgstr "باوباب"

#~ msgid "Could not scan some of the folders contained in “%s”"
#~ msgstr "نمی‌توان تعدادی از شاخه‌های درون «%s» را پویید"

#~ msgid "baobab"
#~ msgstr "baobab"

#~ msgid "_About"
#~ msgstr "_درباره"

#~ msgid ""
#~ "A simple application which can scan either specific folders (local or "
#~ "remote) or volumes and give a graphical representation including each "
#~ "directory size or percentage."
#~ msgstr ""
#~ "یک برنامه ساده که می‌تواند هم پوشه‌های مشخصی (محلی و یا دوردست) و هم جلدها "
#~ "را پایش کند و یک نمایش گرافیکی شامل حجم شاخه‌ها یا درصد آنها را ارایه کند."

#~ msgid "Scan Remote Folder…"
#~ msgstr "پویش پوشه دوردست..."

#~ msgid "Maximum depth"
#~ msgstr "بیشینه عمق"

#~ msgid "The maximum depth drawn in the chart from the root"
#~ msgstr "بیشنه‌ی عمق کشیده شده در نمودار از طریق ریشه"

#~ msgid "Chart model"
#~ msgstr "مدل نمودار"

#~ msgid "Set the model of the chart"
#~ msgstr "تنظیم مدل نمودار"

#~ msgid "Chart root node"
#~ msgstr "ریشه گره‌ی نمودار"

#~ msgid "Set the root node from the model"
#~ msgstr "تنظیم ریشه‌ی گره نمودار از مدل"

#~ msgid "Main volume"
#~ msgstr "جلد اصلی"

#~ msgid "Usage"
#~ msgstr "نحوه استفاده"

#~ msgid "_Analyzer"
#~ msgstr "_تحلیل‌گر"

#~ msgid "Scan F_older…"
#~ msgstr "پویش _پوشه..."

#~ msgid "Scan Remote Fo_lder…"
#~ msgstr "پویش پ_وشه دوردست..."

#~ msgid "_View"
#~ msgstr "_نمایش"

#~ msgid "_Reload"
#~ msgstr "_بارگیری مجدد"

#~ msgid "_Expand All"
#~ msgstr "_باز کردن همه"

#~ msgid "_Collapse All"
#~ msgstr "_جمع‌کردن همه"

#~ msgid "Scan Folder"
#~ msgstr "پویش پوشه"
